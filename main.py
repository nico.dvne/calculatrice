OPERATEUR_ADDITION = '+'
OPERATEUR_SOUSTRACTION = '-'
OPERATEUR_MULTIPLICATION = '*'
OPERATEUR_DIVISION = '/'

OPERATEURS = ['+', '-', '*', '/']



def isOperateurOK(operateur):
    return operateur in OPERATEURS

def isAddition(operateur):
    return OPERATEUR_ADDITION == operateur

def isSoustraction(operateur):
    return OPERATEUR_SOUSTRACTION == operateur

def isMultiplication(operateur):
    return OPERATEUR_MULTIPLICATION == operateur

def isDivision(operateur):
    return OPERATEUR_DIVISION == operateur

def calcul(operande1, operande2, operateur):
    if not isOperateurOK(operateur):
        return False

    operande1 = int(operande1)
    operande2 = int(operande2)
    
    if(isAddition(operateur)):
        return operande1 + operande2

    if(isSoustraction(operateur)):
        return operande1 - operande2

    if(isMultiplication(operateur)):
        return operande1 * operande2

    if(isDivision(operateur)):
        return operande1 / operande2

def getIndiceOperateurPrioritaire(array_saisie):
    for i in range(0, len(array_saisie)):
        if( array_saisie[i] == OPERATEUR_DIVISION or array_saisie[i] == OPERATEUR_MULTIPLICATION):
            return i
    
    return 1

def initValueTab():
    array_saisie = []
    saisie_utilisateur = ''

    while(saisie_utilisateur != '='):
        print("Veuillez saisie une valeur numérique: ")
        saisie_utilisateur = input()
        while (not saisie_utilisateur.isnumeric()):
            print("Veuillez saisir une valeur numerique")
            saisie_utilisateur = input()
        
        array_saisie.append(int(saisie_utilisateur))

        print("Veuillez saisir une opérande, et = pour avoir le résultat")
        saisie_utilisateur = input()
        while(not isOperateurOK(saisie_utilisateur) and not saisie_utilisateur == '=') : 
            print("Veuillez saisir une opérande et = pour avoir le résultat")
            saisie_utilisateur = input()
        
        if saisie_utilisateur != '=':
            array_saisie.append(saisie_utilisateur)

    return array_saisie


def realiserOperationPrioritaire(operation):
    operandeGauche = operation[getIndiceOperateurPrioritaire(operation) -1]
    operandeDroite = operation[getIndiceOperateurPrioritaire(operation) + 1]
    operateur = operation[getIndiceOperateurPrioritaire(operation)]

    return calcul(operandeGauche, operandeDroite, operateur)

def realiserOperation(operation):
    operandeGauche = operation[0]
    operateur = operation[1]
    operandeDroite = operation[2]

    return calcul(operandeGauche, operandeDroite, operateur)

def reecrireOperation(operation, indiceOperateur, resultatCalculPrecedent):
    operation[indiceOperateur - 1] = resultatCalculPrecedent

    for i in range(0, 2) :
        operation.pop(indiceOperateur)

    return operation



array_value = initValueTab()
while(len(array_value) != 1):

    indice_disparition = getIndiceOperateurPrioritaire(array_value)
    result = 0

    if ( -1 != indice_disparition):
        result = realiserOperationPrioritaire(array_value)
    else:
        result = realiserOperation(array_value)

    reecrireOperation(array_value, indice_disparition, result)

    
print(array_value[0])